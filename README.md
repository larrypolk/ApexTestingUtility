# ApexTestingUtility

A simple utility for cleaner `try/catch` unit testing. This was presented at the Chicago Salesforce User Group's December 2017 Multi-Topic Meeting.


## Introduction

The goal of this talk is to demonstrate a novel way to streamline and clarify `try/catch unit` testing in Apex. Using this approach will help you get cleaner Apex code and easier, more targeted unit testing of your `catch` blocks by providing a way to mock both standard and custom exceptions without cluttering your production code with `Testing.isRunningTesting()` blocks. For a more in-depth walkthrough, take a look at the [details page](docs/details.md).

## Standard way to test

When writing Apex code, in order to test one or more `catch` blocks associated with a `try` block, you would need to do something like this:

```apex
public with sharing class Example {
    public static Boolean dmlTestFlag = false;
    public static Boolean emailTestFlag = false;
    public static Boolean customTestFlag = false;
    public static Boolean genericTestFlag = false;

    // Error Message Strings
    public static final String EXCEPT_DML = 'You broke the database!';
    public static final String EXCEPT_EMAIL = 'No email for you today!';
    public static final String EXCEPT_CUSTOM = 'Your special exception happened!';
    public static final String EXCEPT_GENERIC = 'An exception you did not anticipate happened!';

    public static void exampleMethod() {
        try {
            // Processing code

            if (Test.isRunningTest()) {
                if (dmlTestFlag) {
                    throw new DmlException(EXCEPT_DML);
                } else if (emailTestFlag) {
                    throw new EmailException(EXCEPT_EMAIL);
                } else if (customTestFlag) {
                    throw new Exceptions.YourCustomException(EXCEPT_CUSTOM);
                } else if (genericTestFlag) {
                    throw new Exceptions.YourBasicException(EXCEPT_GENERIC);
                }
            }
        } catch (DmlException dmle) {
            // Drat! How do you unit test this?
        } catch (EmailException ee) {
            // Or this?
        } catch (Exceptions.YourCustomException yce) {
            // Or this?
        } catch (Exception e) {
            // Or finally this?
        }
    }
}
```
This isn't ideal for several reasons the most important of which is you've added a sizable amount of test-specific code to your production code base. It gets worse when you consider you need to do this for every class you want to test, all for no other purpose than _testing_.

Enter the Apex Testing Utility approach.

## Testing using the utility

Let's reconsider the previous example. It's possible to thoroughly test all the catch blocks _without_ introducing all the extraneous testing code. 

```apex
public with sharing class Example {
    // Error Message Strings
    public static final String EXCEPT_DML = 'You broke the database!';
    public static final String EXCEPT_EMAIL = 'No email for you today!';
    public static final String EXCEPT_CUSTOM = 'Your special exception happened!';
    public static final String EXCEPT_BASIC = 'An exception you did not anticipate happened!';

    public static void exampleMethod() {
        try {
            // Processing code

            TestingUtility.mockTestError();
        } catch (DmlException dmle) {
            // How do you unit test this? Easily, using the TestingUtility!
        } catch (EmailException ee) {
            // Or this?  Easily, using the TestingUtility!
        } catch (Exceptions.YourCustomException yce) {
            // Or this?  Easily, using the TestingUtility!
        } catch (Exception e) {
            // Or finally this?  Easily, using the TestingUtility!
        }
    }
}
```

This is much cleaner. It no longer requires multiple testing flags or the `Test.isRunningTest()` block. All that is replaced by the one new line: `TestingUtility.mockTestError();`.

## The TestingUtility class

The Testing Utility is a simple utility class ([`TestingUtility.cls`](src/classes/TestingUtility.cls)) that abstracts your testing code away from the class being tested. It's a centralized methodology for streamlining `try`/`catch` testing in all your classes.

### What are the benefits?

There are a few benefits to this approach, including

* Establishes a general pattern for all `try`/`catch` unit testing
* Provides a way to test all `catch` blocks for a given `try` block
* No more testing flags in your production code
* Used by including a single line of code in your `try` block
* Moves all test setup and control into your test classes (where it belongs!)

Let's take a look at an example.

## Example usage

What does a test using the utility look like? Consider the following code.

### Production code
```apex
public with sharing class Example {
    // Error Message Strings
    public static final String EXCEPT_DML = 'You broke the database!';
    public static final String EXCEPT_EMAIL = 'No email for you today!';
    public static final String EXCEPT_CUSTOM = 'Your special exception happened!';
    public static final String EXCEPT_BASIC = 'An exception you did not anticipate happened!';

    public static void exampleMethod() {
        try {
            // Processing code

            TestingUtility.mockTestError();
        } catch (DmlException dmle) {
            // How do you unit test this? Easily, using the TestingUtility!
        } catch (EmailException ee) {
            // Or this?  Easily, using the TestingUtility!
        } catch (Exceptions.YourCustomException yce) {
            // Or this?  Easily, using the TestingUtility!
        } catch (Exception e) {
            // Or finally this?  Easily, using the TestingUtility!
        }
    }
}
```

### Unit test code

```apex
@isTest
public with sharing class ExampleTest {
    @isTest
    public static void testExampleMethod() {
        Test.startTest();
        
        TestingUtility.forceError = true;    // Tells the TestingUtility to force an exception.
        
        // Set up the exception for the catch block you want to test.
        String yourMessage = 'Your message goes here...';
        Exception yourException = new YourExceptionType(yourMessage);    // The exception you want to test.
        TestingUtility.exceptionToMock = yourException;    // Tells the TestingUtility to throw this exception.
        
        try {
            Example.exampleMethod();    // Call your production code here...
        } catch (Exception e) {
            // Test your exception code here. In this example, you would be testing the catch block associated with
            // the exception type you setup in the previous lines.
        }
        
        Test.stopTest();
    }
}
```

That's really all there is to it. 

## API (If you can call it that...)

The Testing Utility ([`TestingUtility.cls`](src/classes/TestingUtility.cls)) contains three methods:

### `mockTestError()`
Include this in your production code's `try` block. You only need to include it once.

### `mockTestError(Exception e)`
Include this in your unit test code. Provides a way to mock a specific exception from your unit test without changing the value of the class' variable.

### `reset()`
Include this in your unit test code. Resets the class' variables back to their default values. Useful if you are testing multiple exception types from within the same test method.


Now you can benefit from cleaner production code without sacrificing code coverage. Happy testing!

