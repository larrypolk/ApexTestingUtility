# ApexTestingUtility

A simple utility for cleaner `try/catch` unit testing. This was presented at the Chicago Salesforce User Group's December 2017 Multi-Topic Meeting.

### Table of Contents

* [Introduction](#introduction)
* [A Simple Example](#a-simple-example)
* [A Slightly More Complex Example](#a-slightly-more-complex-example)
* [The Slightly More Complex Example, Simplified](#the-slightly-more-complex-example-simplified)
* [The TestingUtility Class](#the-testingutility-class)
* [An Example Utility Test](#an-example-utility-test)
* [Summary](#summary)

## Introduction

The goal of this talk is to demonstrate a novel way to streamline and clarify `try/catch unit` testing in Apex. Using this approach will help you get cleaner Apex code and easier, more targeted unit testing of your `catch` blocks by providing a way to mock both standard and custom exceptions without cluttering your production code with `Testing.isRunningTesting()` blocks.

## A Simple Example

Let's say you have the following example ([`SimpleExample.cls`](src/classes/SimpleExample.cls)):

```apex
public with sharing class SimpleExample {
    public static void simpleMethod() {
        try {
            // Processing code
        } catch (Exception e) {
            // Uh oh! How do you unit test this?
        }
    }
}
```

How do you go about getting code coverage for the `catch` block? Generally, the standard approach to this problem is to do the following ([`SimpleExample.cls`](src/classes/SimpleExample.cls)):

```apex
public with sharing class SimpleExample {
    public static Boolean testFlag = false;

    public static final String EXCEPT_DML = 'You broke the database!';

    public static void simpleMethodWithTesting() {
        try {
            // Processing code

            if (Test.isRunningTest() && testFlag) {
                throw new DmlException(EXCEPT_DML);
            }
        } catch (Exception e) {
            // Great! Now able to test this code
        }
    }
}
```

The unit test for this example might look something like this ([`SimpleExampleTest.cls`](src/classes/SimpleExampleTest.cls)):

```apex
@isTest
public with sharing class SimpleExampleTest {
    @isTest
    public static void testSimpleMethodWithTesting() {
        Test.startTest();

        SimpleExample.testFlag = true;    // Set the source file's testing flag to true so the error is thrown.

        try {
            SimpleExample.simpleMethodWithTesting();
        } catch (Exception e) {
            DmlException expExcept = new DmlException();    // Expected Exception Type

            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(SimpleExample.EXCEPT_DML), 'The exception message does not contain the expected text.');
        }

        Test.stopTest();
    }
}
```

That's not so bad, right?. While it's probably not a great idea to have testing code intermixed with production code, it's not too terrible and it gets the job done. But what happens when the situation isn't quite so simple?

## A Slightly More Complex Example

Consider a similar but slightly more complex scenario, shown here ([`ComplexExample.cls`](src/classes/ComplexExample.cls)):

```apex
public with sharing class ComplexExample {
    public static void complexMethod() {
        try {
            // Processing code
        } catch (DmlException dmle) {
            // Drat! How do you unit test this?
        } catch (EmailException ee) {
            // Or this?
        } catch (Exceptions.YourCustomException yce) {
            // Or this?
        // .
        // .
        // .
        } catch (Exception e) {
            // Or finally this?
        }
    }
}
```

Following the approach described in the simple example, you'd end up with something like this ([`ComplexExample.cls`](src/classes/ComplexExample.cls)):

```apex
public with sharing class ComplexExample {
    public static Boolean dmlTestFlag = false;
    public static Boolean emailTestFlag = false;
    public static Boolean customTestFlag = false;
    public static Boolean genericTestFlag = false;

    // Error Message Strings
    public static final String EXCEPT_DML = 'You broke the database!';
    public static final String EXCEPT_EMAIL = 'No email for you today!';
    public static final String EXCEPT_CUSTOM = 'Your special exception happened!';
    public static final String EXCEPT_GENERIC = 'An exception you did not anticipate happened!';

    public static void complexMethodWithTesting() {
        try {
            // Processing code

            if (Test.isRunningTest()) {
                if (dmlTestFlag) {
                    throw new DmlException(EXCEPT_DML);
                } else if (emailTestFlag) {
                    throw new EmailException(EXCEPT_EMAIL);
                } else if (customTestFlag) {
                    throw new Exceptions.YourCustomException(EXCEPT_CUSTOM);
                } else if (genericTestFlag) {
                    throw new Exceptions.YourBasicException(EXCEPT_GENERIC);
                }
            }
        } catch (DmlException dmle) {
            // Drat! How do you unit test this?
        } catch (EmailException ee) {
            // Or this?
        } catch (Exceptions.YourCustomException yce) {
            // Or this?
        } catch (Exception e) {
            // Or finally this?
        }
    }
}
```

Clearly, things are getting out of hand rather quickly. You've now added a sizable amount of test-specific code to your production code base. It gets worse when you consider you need to do this for every class you want to test. All this for no other purpose than _testing_.

The unit test for this example ([`ComplexExampleTest.cls`](src/classes/ComplexExampleTest.cls)) is very similar to the simple example's unit test, it's just much, much longer.

## The Slightly More Complex Example, Simplified

Let's reconsider the previous example. It's possible to thoroughly test all the catch blocks _without_ introducing all the extraneous testing code ([`UtilityExample.cls`](src/classes/UtilityExample.cls)). 

```apex
public with sharing class UtilityExample {
    // Error Message Strings
    public static final String EXCEPT_DML = 'You broke the database!';
    public static final String EXCEPT_EMAIL = 'No email for you today!';
    public static final String EXCEPT_CUSTOM = 'Your special exception happened!';
    public static final String EXCEPT_BASIC = 'An exception you did not anticipate happened!';

    public static void utilityMethodWithTesting() {
        try {
            // Processing code

            TestingUtility.mockTestError();
        } catch (DmlException dmle) {
            // How do you unit test this? Easily, using the TestingUtility!
        } catch (EmailException ee) {
            // Or this?  Easily, using the TestingUtility!
        } catch (Exceptions.YourCustomException yce) {
            // Or this?  Easily, using the TestingUtility!
        } catch (Exception e) {
            // Or finally this?  Easily, using the TestingUtility!
        }
    }
}
```

This is much cleaner. It no longer requires multiple testing flags or the `Test.isRunningTest()` block. All that is replaced by the one new line: `TestingUtility.mockTestError();`.

## The TestingUtility Class

The Testing Utility is a simple utility class ([`TestingUtility.cls`](src/classes/TestingUtility.cls)) that abstracts your testing code away from the class being tested. It's a centralized methodology for streamlining `try`/`catch` testing in all your classes.

```apex
public with sharing class TestingUtility {
    // NEVER set these values here; instead, set them in the calling Test code where you
    // want to test one or more exceptions.
    public static Boolean forceError = false;          // !!!! NEVER CHANGE THIS HERE !!!!
    public static Exception exceptionToMock = null;    // !!!! NEVER CHANGE THIS HERE !!!!

    // Message Strings
    public static final String ERR_MOCK_MESSAGE = 'This is a Mock Error. It is used for testing purposes only.';

    // Constructor
    public TestingUtility() {}

    public static void mockTestError() {
        if (Test.isRunningTest() && forceError) {
            if (exceptionToMock == null) {
                throw new MockTestException(ERR_MOCK_MESSAGE);
            }

            throw exceptionToMock;
        }
    }

    public static void mockTestError(Exception e) {
        if (Test.isRunningTest() && forceError) {
            throw e;
        }
    }
    
    public static void reset() {
        forceError = false;
        exceptionToMock = null;
    }

    public with sharing class GenericException extends Exception {}
    public with sharing class MockTestException extends Exception {}
}
```

### What are the benefits?

There are a few benefits to this approach, including

* Establishes a general pattern for all `try`/`catch` unit testing
* Provides a way to test all `catch` blocks for a given `try` block
* No more testing flags in your production code
* Used by including a single line of code in your `try` block
* Moves all test setup and control into your test classes (where it belongs!)

Let's take a look at an example.

## An Example Utility Test

What does a test using the utility look like? Consider the following code ([`UtilityExampleTest.cls`](src/classes/UtilityExampleTest.cls)).

```apex
@isTest
public with sharing class UtilityExampleTest {
    @isTest
    public static void testUtilityMethodWithTesting() {
        Test.startTest();

        TestingUtility.forceError = true;    // Set the flag in the TestingUtility to force testing mode.

        // Test the first exception block - DmlException
        String expMessage = UtilityExample.EXCEPT_DML;    // Expected Exception message - DmlException
        Exception expExcept = new DmlException(expMessage);    // Expected Exception - DmlException
        TestingUtility.exceptionToMock = expExcept;

        try {
            UtilityExample.utilityMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        // Test the second exception block - EmailException
        expMessage = UtilityExample.EXCEPT_EMAIL;    // Expected Exception message - EmailException
        expExcept = new EmailException(expMessage);    // Expected Exception - EmailException
        TestingUtility.exceptionToMock = expExcept;

        try {
            UtilityExample.utilityMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        // Test the third exception block - YourCustomException
        expMessage = UtilityExample.EXCEPT_CUSTOM;    // Expected Exception message - YourCustomException
        expExcept = new Exceptions.YourCustomException(expMessage);    // Expected Exception - YourCustomException
        TestingUtility.exceptionToMock = expExcept;

        try {
            UtilityExample.utilityMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        // Test the fourth exception block - Generic Exception
        expMessage = TestingUtility.ERR_MOCK_MESSAGE;    // Expected Exception message - MockTestException
        expExcept = new TestingUtility.MockTestException(expMessage);    // Expected Exception - MockTestException
        TestingUtility.exceptionToMock = null;

        try {
            UtilityExample.utilityMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        Test.stopTest();
    }
}
```

And that's pretty much it. It's easy to see how using the Testing Utility class can help you clean up your production code.

## Summary

As shown in the examples, the Apex Testing Utility provides several benefits including: establishing a general pattern for unit testing your `try`/`catch` blocks, an easy means to test all `catch` blocks for a `try` block, and removing all test setup and control into your test classes. While it is certainly not a perfect solution, it is just another way to approach Apex unit testing by helping you abstract the required test setup and control code out of your production code.. Use it in conjunction with your established toolset to achieve better, cleaner, more targeted unit testing.
