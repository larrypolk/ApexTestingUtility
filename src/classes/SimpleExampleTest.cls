@isTest
public with sharing class SimpleExampleTest {
    @isTest
    public static void testSimpleMethod() {
        Test.startTest();

        SimpleExample.simpleMethod();

        Test.stopTest();
    }

    @isTest
    public static void testSimpleMethodWithTesting() {
        Test.startTest();

        SimpleExample.testFlag = true;    // Set the source file's testing flag to true so the error is thrown.

        try {
            SimpleExample.simpleMethodWithTesting();
        } catch (Exception e) {
            DmlException expExcept = new DmlException();    // Expected Exception Type

            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(SimpleExample.EXCEPT_DML), 'The exception message does not contain the expected text.');
        }

        Test.stopTest();
    }
}
