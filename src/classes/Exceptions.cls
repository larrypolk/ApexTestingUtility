public with sharing class Exceptions {

    public with sharing class YourBasicException extends Exception {}

    public with sharing class YourCustomException extends Exception {}

}
