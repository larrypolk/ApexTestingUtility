public with sharing class SimpleExample {
    public static Boolean testFlag = false;

    public static final String EXCEPT_DML = 'You broke the database!';

    public static void simpleMethod() {
        try {
            // Processing code
        } catch (Exception e) {
            // Uh oh! How do you unit test this?
        }
    }

    public static void simpleMethodWithTesting() {
        try {
            // Processing code

            if (Test.isRunningTest() && testFlag) {
                throw new DmlException(EXCEPT_DML);
            }
        } catch (Exception e) {
            // Great! Now able to test this code
        }
    }
}
