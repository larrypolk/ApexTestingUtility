public with sharing class UtilityExample {
    // Error Message Strings
    public static final String EXCEPT_DML = 'You broke the database!';
    public static final String EXCEPT_EMAIL = 'No email for you today!';
    public static final String EXCEPT_CUSTOM = 'Your special exception happened!';
    public static final String EXCEPT_BASIC = 'An exception you did not anticipate happened!';

    public static void utilityMethodWithTesting() {
        try {
            // Processing code

            TestingUtility.mockTestError();
        } catch (DmlException dmle) {
            // How do you unit test this? Easily, using the TestingUtility!
        } catch (EmailException ee) {
            // Or this?  Easily, using the TestingUtility!
        } catch (Exceptions.YourCustomException yce) {
            // Or this?  Easily, using the TestingUtility!
        } catch (Exception e) {
            // Or finally this?  Easily, using the TestingUtility!
        }
    }
}
