public with sharing class ComplexExample {
    public static Boolean dmlTestFlag = false;
    public static Boolean emailTestFlag = false;
    public static Boolean customTestFlag = false;
    public static Boolean genericTestFlag = false;

    // Error Message Strings
    public static final String EXCEPT_DML = 'You broke the database!';
    public static final String EXCEPT_EMAIL = 'No email for you today!';
    public static final String EXCEPT_CUSTOM = 'Your special exception happened!';
    public static final String EXCEPT_GENERIC = 'An exception you did not anticipate happened!';

    public static void complexMethod() {
        try {
            // Processing code
        } catch (DmlException dmle) {
            // Drat! How do you unit test this?
        } catch (EmailException ee) {
            // Or this?
        } catch (Exceptions.YourCustomException yce) {
            // Or this?
        // .
        // .
        // .
        } catch (Exception e) {
            // Or finally this?
        }
    }

    public static void complexMethodWithTesting() {
        try {
            // Processing code

            if (Test.isRunningTest()) {
                if (dmlTestFlag) {
                    throw new DmlException(EXCEPT_DML);
                } else if (emailTestFlag) {
                    throw new EmailException(EXCEPT_EMAIL);
                } else if (customTestFlag) {
                    throw new Exceptions.YourCustomException(EXCEPT_CUSTOM);
                } else if (genericTestFlag) {
                    throw new Exceptions.YourBasicException(EXCEPT_GENERIC);
                }
            }
        } catch (DmlException dmle) {
            // Drat! How do you unit test this?
        } catch (EmailException ee) {
            // Or this?
        } catch (Exceptions.YourCustomException yce) {
            // Or this?
        } catch (Exception e) {
            // Or finally this?
        }
    }
}
