/**
 * @author Larry Polk
 * @date 2017
 *
 * @description A utility to streamline try/catch unit testing in Apex.
 */
public with sharing class TestingUtility {
    // NEVER set these values here; instead, set them in the calling Test code where you
    // want to test one or more exceptions.
    public static Boolean forceError = false;          // !!!! NEVER CHANGE THIS HERE !!!!
    public static Exception exceptionToMock = null;    // !!!! NEVER CHANGE THIS HERE !!!!

    // Message Strings
    public static final String ERR_MOCK_MESSAGE = 'This is a Mock Error. It is used for testing purposes only.';

    // Constructor
    public TestingUtility() {}

    /**
     *  @description Throws a MockTestException for testing catch blocks.
     *  @return null
     *
     *  @example
     *  TestingUtility.mockTestError();
     */
    public static void mockTestError() {
        if (Test.isRunningTest() && forceError) {
            if (exceptionToMock == null) {
                throw new MockTestException(ERR_MOCK_MESSAGE);
            }

            throw exceptionToMock;
        }
    }

    /**
     * @description Throws the exception provided when calling the method.
     * @param e The specific exception to throw for testing catch blocks.
     * @return null
     *
     * @example
     * TestingUtility.mockTestError(new DmlException('Error message string'));
     */
    public static void mockTestError(Exception e) {
        if (Test.isRunningTest() && forceError) {
            throw e;
        }
    }

    /**
     * @description Resets the variables in the TestingUtility back to their default values.
     *
     * @example
     * TestingUtility.reset();
     */
    public static void reset() {
        forceError = false;
        exceptionToMock = null;
    }

    /**
     * @description A custom generic exception used when calling the TestingUtility.mockTestError() method; this
     * is useful for testing an Exception type other than the default MockTestException type.
     */
    public with sharing class GenericException extends Exception {}

    /**
     * @description A custom exception used when calling the TestingUtility.mockTestError() method
     * without a parameter.
     */
    public with sharing class MockTestException extends Exception {}
}
