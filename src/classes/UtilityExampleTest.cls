@isTest
public with sharing class UtilityExampleTest {
    @isTest
    public static void testUtilityMethodWithTesting() {
        Test.startTest();

        TestingUtility.forceError = true;    // Set the flag in the TestingUtility to force testing mode.

        // Test the first exception block - DmlException
        String expMessage = UtilityExample.EXCEPT_DML;    // Expected Exception message - DmlException
        Exception expExcept = new DmlException(expMessage);    // Expected Exception - DmlException
        TestingUtility.exceptionToMock = expExcept;

        try {
            UtilityExample.utilityMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        // Test the second exception block - EmailException
        expMessage = UtilityExample.EXCEPT_EMAIL;    // Expected Exception message - EmailException
        expExcept = new EmailException(expMessage);    // Expected Exception - EmailException
        TestingUtility.exceptionToMock = expExcept;

        try {
            UtilityExample.utilityMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        // Test the third exception block - YourCustomException
        expMessage = UtilityExample.EXCEPT_CUSTOM;    // Expected Exception message - YourCustomException
        expExcept = new Exceptions.YourCustomException(expMessage);    // Expected Exception - YourCustomException
        TestingUtility.exceptionToMock = expExcept;

        try {
            UtilityExample.utilityMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        // Test the fourth exception block - Generic Exception
        expMessage = TestingUtility.ERR_MOCK_MESSAGE;    // Expected Exception message - MockTestException
        expExcept = new TestingUtility.MockTestException(expMessage);    // Expected Exception - MockTestException
        TestingUtility.exceptionToMock = null;

        try {
            UtilityExample.utilityMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        Test.stopTest();
    }
}
