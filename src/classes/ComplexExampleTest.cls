@isTest
public with sharing class ComplexExampleTest {
    @isTest
    public static void testComplexMethod() {
        Test.startTest();

        ComplexExample.complexMethod();

        Test.stopTest();
    }

    @isTest
    public static void testComplexMethodWithTesting() {
        Test.startTest();

        // Test the first exception block - DmlException
        ComplexExample.dmlTestFlag = true;
        String expMessage = ComplexExample.EXCEPT_DML;    // Expected Exception message - DmlException
        Exception expExcept = new DmlException(expMessage);    // Expected Exception - DmlException

        try {
            ComplexExample.complexMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        // Test the second exception block - EmailException
        ComplexExample.dmlTestFlag = false;    // Don't forget to reset this or you'll get the wrong exception type.
        ComplexExample.emailTestFlag = true;
        expMessage = ComplexExample.EXCEPT_EMAIL;        // Expected Exception message - EmailException
        expExcept = new EmailException(expMessage);    // Expected Exception - EmailException

        try {
            ComplexExample.complexMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        // Test the third exception block - YourCustomException
        ComplexExample.emailTestFlag = false;    // Don't forget to reset this or you'll get the wrong exception type.
        ComplexExample.customTestFlag = true;
        expMessage = ComplexExample.EXCEPT_EMAIL;        // Expected Exception message - YourCustomException
        expExcept = new Exceptions.YourCustomException(expMessage);    // Expected Exception - YourCustomException

        try {
            ComplexExample.complexMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        // Test the fourth exception block - Generic Exception
        ComplexExample.customTestFlag = false;    // Don't forget to reset this or you'll get the wrong exception type.
        ComplexExample.genericTestFlag = true;
        expMessage = ComplexExample.EXCEPT_GENERIC;        // Expected Exception message - YourCustomException
        expExcept = new Exceptions.YourCustomException(expMessage);    // Expected Exception - YourCustomException

        try {
            ComplexExample.complexMethodWithTesting();
        } catch (Exception e) {
            System.assertEquals(expExcept.getTypeName(), e.getTypeName(), 'Exception types do not match.');
            System.assert(e.getMessage().contains(expMessage), 'The exception message does not contain the expected text.');
        }

        Test.stopTest();
    }
}
